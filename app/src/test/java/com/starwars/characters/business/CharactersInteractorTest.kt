package com.starwars.characters.business

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.starwars.characters.business.models.Character
import com.starwars.characters.business.models.PagedCharactersList
import com.starwars.characters.data.repositories.CharactersRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CharactersInteractorTest {

    @Mock
    private lateinit var repository: CharactersRepository

    @InjectMocks
    private lateinit var interactor: CharactersInteractor

    @Before
    fun setUp() {
        val pagedList = PagedCharactersList(false, CHARACTERS_LIST)
        whenever(repository.searchCharacter(any(), any())).thenReturn(Single.just(pagedList))
    }

    @Test
    fun `search character result has not next page return empty list`() {
        interactor.searchCharacter(ANY_STRING_1, 1)
            .test()
            .await()

        interactor.searchCharacter(ANY_STRING_1, 2)
            .test()
            .assertValue { it.isEmpty() }
    }

    @Test
    fun `search character correct result on new query`() {
        interactor.searchCharacter(ANY_STRING_1, 1)
            .test()
            .await()

        interactor.searchCharacter(ANY_STRING_2, 1)
            .test()
            .assertValue { it == CHARACTERS_LIST }
    }

    @Test
    fun `search character empty result on blank query`() {
        interactor.searchCharacter("    ", 1)
            .test()
            .assertValue { it.isEmpty() }

        interactor.searchCharacter("", 1)
            .test()
            .assertValue { it.isEmpty() }
    }

    companion object {
        private const val ANY_STRING_1 = "Luke"
        private const val ANY_STRING_2 = "R2D2"

        private val CHARACTERS_LIST = listOf(
            Character(
                name = ANY_STRING_1,
                height = "",
                mass = "",
                hairColor = "",
                skinColor = "",
                eyeColor = "",
                birthYear = "",
                gender = ""
            ),
            Character(
                name = ANY_STRING_2,
                height = "",
                mass = "",
                hairColor = "",
                skinColor = "",
                eyeColor = "",
                birthYear = "",
                gender = ""
            )
        )
    }
}