package com.starwars.characters.business

import com.starwars.characters.business.models.Character
import com.starwars.characters.data.repositories.CharactersRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class CharactersInteractor @Inject constructor(private val repository: CharactersRepository) {

    private var currentSearchQuery = ""
    private var hasNextForCurrentQuery = true

    fun searchCharacter(query: String, page: Int): Single<List<Character>> {
        if (currentSearchQuery != query) {
            currentSearchQuery = query
            hasNextForCurrentQuery = true
        }

        return if (hasNextForCurrentQuery && query.isNotBlank()) {
            repository.searchCharacter(query, page)
                .doOnSuccess { hasNextForCurrentQuery = it.hasNext }
                .map { it.value }
        } else {
            Single.just(emptyList())
        }
    }

    fun saveCharacter(character: Character): Completable {
        return repository.saveCharacter(character)
    }

    fun getCharacterByName(name: String): Single<Character> {
        return repository.findSavedCharacterByName(name)
    }

    fun getRecentCharacters(): Single<List<Character>> {
        return repository.getAllSavedCharacters()
    }
}