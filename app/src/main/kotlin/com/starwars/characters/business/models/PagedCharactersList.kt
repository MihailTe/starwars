package com.starwars.characters.business.models

class PagedCharactersList(
    val hasNext: Boolean,
    val value: List<Character>
)