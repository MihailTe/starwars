package com.starwars.characters.data.repositories

import com.starwars.characters.business.models.Character
import com.starwars.characters.business.models.PagedCharactersList
import io.reactivex.Completable
import io.reactivex.Single

interface CharactersRepository {

    fun searchCharacter(query: String, page: Int): Single<PagedCharactersList>

    fun saveCharacter(character: Character): Completable

    fun getAllSavedCharacters(): Single<List<Character>>

    fun findSavedCharacterByName(name: String): Single<Character>
}