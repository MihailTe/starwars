package com.starwars.characters.data.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Swapi {

    @GET("people")
    fun searchCharacter(
        @Query("search") query: String,
        @Query("page") page: Int
    ): Single<PagedCharactersResponse>
}