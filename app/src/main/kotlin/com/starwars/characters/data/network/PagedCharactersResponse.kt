package com.starwars.characters.data.network

class PagedCharactersResponse(
    val next: String?,
    val results: List<Result>?
) {

    class Result(
        val name: String,
        val height: String?,
        val mass: String?,
        val hairColor: String?,
        val skinColor: String?,
        val eyeColor: String?,
        val birthYear: String?,
        val gender: String?
    )
}