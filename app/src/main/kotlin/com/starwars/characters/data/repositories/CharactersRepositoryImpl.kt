package com.starwars.characters.data.repositories

import com.starwars.characters.business.models.Character
import com.starwars.characters.business.models.PagedCharactersList
import com.starwars.characters.data.db.CharactersDao
import com.starwars.characters.data.network.Swapi
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers.io
import javax.inject.Inject

class CharactersRepositoryImpl @Inject constructor(
    private val api: Swapi,
    private val dao: CharactersDao,
    private val mapper: CharactersMapper
) : CharactersRepository {

    override fun searchCharacter(query: String, page: Int): Single<PagedCharactersList> {
        return api.searchCharacter(query, page)
            .map { mapper.mapToPagedCharacters(it) }
    }

    override fun saveCharacter(character: Character): Completable {
        return Completable
            .fromAction {
                val entity = mapper.mapToEntity(character)
                dao.saveCharacter(entity)
            }
            .subscribeOn(io())
    }

    override fun getAllSavedCharacters(): Single<List<Character>> {
        return dao.getAllCharactersSortedByTimestamp()
            .map { mapper.mapToCharacters(it) }
            .subscribeOn(io())
    }

    override fun findSavedCharacterByName(name: String): Single<Character> {
        return dao.findCharacterByName(name)
            .map { mapper.mapToCharacter(it) }
            .subscribeOn(io())
    }
}