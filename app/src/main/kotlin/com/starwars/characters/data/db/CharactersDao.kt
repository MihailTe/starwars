package com.starwars.characters.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface CharactersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCharacter(character: CharacterEntity)

    @Query("SELECT * FROM character_entity ORDER BY timestamp DESC")
    fun getAllCharactersSortedByTimestamp(): Single<List<CharacterEntity>>

    @Query("SELECT * FROM character_entity WHERE name = :name")
    fun findCharacterByName(name: String): Single<CharacterEntity>
}