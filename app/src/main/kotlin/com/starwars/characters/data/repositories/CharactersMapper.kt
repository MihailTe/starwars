package com.starwars.characters.data.repositories

import com.starwars.characters.business.models.Character
import com.starwars.characters.business.models.PagedCharactersList
import com.starwars.characters.data.db.CharacterEntity
import com.starwars.characters.data.network.PagedCharactersResponse
import javax.inject.Inject

class CharactersMapper @Inject constructor() {

    fun mapToPagedCharacters(response: PagedCharactersResponse): PagedCharactersList {
        val characters = response
            .results
            .orEmpty()
            .map { mapToCharacter(it) }

        return PagedCharactersList(response.next.isNullOrBlank().not(), characters)
    }

    fun mapToCharacter(response: PagedCharactersResponse.Result): Character {
        return Character(
            name = response.name,
            height = response.height ?: EMPTY_STRING_PLACEHOLDER,
            mass = response.mass ?: EMPTY_STRING_PLACEHOLDER,
            hairColor = response.hairColor ?: EMPTY_STRING_PLACEHOLDER,
            skinColor = response.skinColor ?: EMPTY_STRING_PLACEHOLDER,
            eyeColor = response.eyeColor ?: EMPTY_STRING_PLACEHOLDER,
            birthYear = response.birthYear ?: EMPTY_STRING_PLACEHOLDER,
            gender = response.gender ?: EMPTY_STRING_PLACEHOLDER
        )
    }

    fun mapToCharacter(entity: CharacterEntity): Character {
        return Character(
            name = entity.name,
            height = entity.height ?: EMPTY_STRING_PLACEHOLDER,
            mass = entity.mass ?: EMPTY_STRING_PLACEHOLDER,
            hairColor = entity.hairColor ?: EMPTY_STRING_PLACEHOLDER,
            skinColor = entity.skinColor ?: EMPTY_STRING_PLACEHOLDER,
            eyeColor = entity.eyeColor ?: EMPTY_STRING_PLACEHOLDER,
            birthYear = entity.birthYear ?: EMPTY_STRING_PLACEHOLDER,
            gender = entity.gender ?: EMPTY_STRING_PLACEHOLDER
        )
    }

    fun mapToEntity(character: Character): CharacterEntity {
        return CharacterEntity(
            name = character.name,
            height = character.height,
            mass = character.mass,
            hairColor = character.hairColor,
            skinColor = character.skinColor,
            eyeColor = character.eyeColor,
            birthYear = character.birthYear,
            gender = character.gender,
            timestamp = System.currentTimeMillis()
        )
    }

    fun mapToCharacters(entities: List<CharacterEntity>): List<Character> {
        return entities.map { mapToCharacter(it) }
    }

    companion object {
        private const val EMPTY_STRING_PLACEHOLDER = "n/a"
    }
}