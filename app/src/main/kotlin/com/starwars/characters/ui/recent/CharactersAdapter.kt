package com.starwars.characters.ui.recent

import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.starwars.characters.business.models.Character
import com.starwars.characters.ui.utils.delegates.CharacterAdapterDelegate

class CharactersAdapter(onClick: (character: Character) -> Unit) : ListDelegationAdapter<MutableList<Any>>() {

    init {
        items = mutableListOf()
        delegatesManager.addDelegate(CharacterAdapterDelegate(onClick))
    }

    fun setData(characters: List<Character>) {
        items.clear()
        items.addAll(characters)
        notifyDataSetChanged()
    }
}