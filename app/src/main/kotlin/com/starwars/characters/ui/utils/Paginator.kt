package com.starwars.characters.ui.utils

import android.arch.lifecycle.MutableLiveData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposable

class Paginator<T>(private val stateLiveData: MutableLiveData<Paginator.ScreenState<T>>) {

    private lateinit var requestFactory: (Int) -> Single<List<T>>

    private val firstPage = 1

    private var currentState: State<T> = Empty()
    private var currentPage = 0
    private val currentData = mutableListOf<T>()
    private var disposable: Disposable? = null

    fun setRequestFactory(factory: (Int) -> Single<List<T>>) {
        requestFactory = factory
    }

    fun restart() {
        currentState.restart()
    }

    fun loadNewPage() {
        currentState.loadNewPage()
    }

    fun release() {
        currentState.release()
    }

    private fun loadPage(page: Int) {
        disposable?.dispose()
        disposable = requestFactory.invoke(page)
            .observeOn(mainThread())
            .subscribe(
                { currentState.newData(it) },
                {
                    currentState.fail(it)
                    SimpleLogger.log(it)
                }
            )
    }

    sealed class ScreenState<T> {
        class EmptyProgress<T> : ScreenState<T>()

        class PageProgress<T> : ScreenState<T>()

        class Empty<T> : ScreenState<T>()

        class Error<T>(val throwable: Throwable) : ScreenState<T>()

        class Data<T>(val value: List<T>) : ScreenState<T>()
    }

    private interface State<T> {
        fun restart() = Unit
        fun loadNewPage() = Unit
        fun release() = Unit
        fun newData(data: List<T>) = Unit
        fun fail(error: Throwable) = Unit
    }

    private inner class Empty : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class EmptyProgress : State<T> {

        override fun restart() {
            loadPage(firstPage)
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentState = Data()
                currentData.clear()
                currentData.addAll(data)
                currentPage = firstPage
                stateLiveData.value = ScreenState.Data(currentData)
            } else {
                currentState = EmptyData()
                stateLiveData.value = ScreenState.Empty()
            }
        }

        override fun fail(error: Throwable) {
            currentState = EmptyError()
            stateLiveData.value = ScreenState.Error(error)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class EmptyError : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class EmptyData : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class Data : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun loadNewPage() {
            currentState = PageProgress()
            stateLiveData.value = ScreenState.PageProgress()
            loadPage(currentPage + 1)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class PageProgress : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun newData(data: List<T>) {
            if (data.isNotEmpty()) {
                currentState = Data()
                currentData.addAll(data)
                currentPage++
                stateLiveData.value = ScreenState.Data(currentData)
            } else {
                currentState = AllData()
                stateLiveData.value = ScreenState.Data(currentData)
            }
        }

        override fun fail(error: Throwable) {
            currentState = Data()
            stateLiveData.value = ScreenState.Error(error)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }

    private inner class AllData : State<T> {

        override fun restart() {
            currentState = EmptyProgress()
            stateLiveData.value = ScreenState.EmptyProgress()
            loadPage(firstPage)
        }

        override fun release() {
            currentState = Empty()
            disposable?.dispose()
        }
    }
}