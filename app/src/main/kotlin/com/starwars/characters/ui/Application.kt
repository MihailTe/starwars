package com.starwars.characters.ui

import android.app.Application
import com.starwars.characters.di.Injector

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.initApplicationComponent(this)
    }
}