package com.starwars.characters.ui.utils

import android.util.Log

object SimpleLogger {

    fun log(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message)
    }
}