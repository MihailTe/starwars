package com.starwars.characters.ui.root

import android.support.v4.app.Fragment
import com.starwars.characters.ui.character.CharacterFragment
import com.starwars.characters.ui.recent.RecentFragment
import com.starwars.characters.ui.search.SearchFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class Search : SupportAppScreen() {
        override fun getFragment(): Fragment = SearchFragment()
    }

    class Recent : SupportAppScreen() {
        override fun getFragment(): Fragment = RecentFragment()
    }

    class Character(private val name: String) : SupportAppScreen() {
        override fun getFragment(): Fragment = CharacterFragment.newInstance(name)
    }
}