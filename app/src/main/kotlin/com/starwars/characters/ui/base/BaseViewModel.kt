package com.starwars.characters.ui.base

import android.arch.lifecycle.ViewModel
import com.starwars.characters.ui.utils.SimpleLogger
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {
    private val onNextStub: (Any) -> Unit = {}
    private val onErrorStub: (Throwable) -> Unit = {}
    private val onCompleteStub: () -> Unit = {}

    fun <T : Any> Observable<T>.subscribeBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub,
        onNext: (T) -> Unit = onNextStub
    ): Disposable = subscribe(onNext, wrapWithErrorLogging(onError), onComplete)

    private fun wrapWithErrorLogging(onError: (Throwable) -> Unit): (Throwable) -> Unit = {
        onError(it)
        SimpleLogger.log(it)
    }
}