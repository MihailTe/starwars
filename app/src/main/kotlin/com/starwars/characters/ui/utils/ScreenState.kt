package com.starwars.characters.ui.utils

enum class ScreenState {
    LOADING, CONTENT, ERROR
}