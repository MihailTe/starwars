package com.starwars.characters.ui.root

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.starwars.characters.R
import com.starwars.characters.di.Injector
import kotlinx.android.synthetic.main.activity_root.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class RootActivity : AppCompatActivity() {

    private lateinit var navigationHolder: NavigatorHolder

    private lateinit var viewModel: RootViewModel

    private val navigator: Navigator = SupportAppNavigator(this, R.id.rootContainer)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(Injector.getActivityComponent()) {
            viewModel = ViewModelProviders
                .of(this@RootActivity, getViewModelFactory())
                .get(RootViewModel::class.java)

            navigationHolder = getNavigationHolder()
        }

        setContentView(R.layout.activity_root)

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.search -> viewModel.openSearch()
                R.id.recent -> viewModel.openRecent()
                else -> throw IllegalArgumentException("Not supported menu item id")
            }

            true
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    fun showBottomNavigation() {
        bottomNavigationView.visibility = View.VISIBLE
    }

    fun hideBottomNavigation() {
        bottomNavigationView.visibility = View.GONE
    }
}
