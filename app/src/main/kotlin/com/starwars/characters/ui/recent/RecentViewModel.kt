package com.starwars.characters.ui.recent

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.starwars.characters.business.CharactersInteractor
import com.starwars.characters.business.models.Character
import com.starwars.characters.ui.root.Screens
import com.starwars.characters.ui.utils.ScreenState
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class RecentViewModel @Inject constructor(
    private val router: Router,
    private val charactersInteractor: CharactersInteractor
) : ViewModel() {

    private val characters = MutableLiveData<List<Character>>()
    private val screenState = MutableLiveData<ScreenState>()

    private var disposable = Disposables.empty()

    init {
        loadCharacters()
    }

    fun retry() {
        loadCharacters()
    }

    private fun loadCharacters() {
        screenState.value = ScreenState.LOADING

        disposable = charactersInteractor.getRecentCharacters()
            .observeOn(mainThread())
            .subscribeBy(
                onSuccess = {
                    characters.value = it
                    screenState.value = ScreenState.CONTENT
                }, onError = {
                    screenState.value = ScreenState.ERROR
                })
    }

    fun openCharacter(character: Character) {
        router.navigateTo(Screens.Character(character.name))
    }

    fun getScreenState(): LiveData<ScreenState> = screenState

    fun getCharacters(): LiveData<List<Character>> = characters
}