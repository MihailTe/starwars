package com.starwars.characters.ui.search

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.starwars.characters.business.CharactersInteractor
import com.starwars.characters.business.models.Character
import com.starwars.characters.ui.root.Screens
import com.starwars.characters.ui.utils.Paginator
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val router: Router,
    private val charactersInteractor: CharactersInteractor
) : ViewModel() {

    private val searchState = MutableLiveData<Paginator.ScreenState<Character>>()

    private val queryObservable = BehaviorSubject.create<String>()
    private val disposables = CompositeDisposable()

    private var paginator: Paginator<Character> = Paginator(searchState)

    init {
        disposables += queryObservable
            .distinctUntilChanged()
            .doOnNext { paginator.release() }
            .debounce(DEBOUNCE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
            .observeOn(mainThread())
            .subscribeBy(onNext = { query ->
                paginator.setRequestFactory { charactersInteractor.searchCharacter(query, it) }
                paginator.restart()
            })
    }

    override fun onCleared() {
        disposables.dispose()
        paginator.release()
    }

    fun setQuery(query: String) {
        queryObservable.onNext(query)
    }

    fun isEmptyQuery() = queryObservable.value.isNullOrBlank()

    fun getLastQuery() = queryObservable.value.orEmpty()

    fun retry() {
        paginator.restart()
    }

    fun getScreenState(): LiveData<Paginator.ScreenState<Character>> = searchState

    fun openCharacter(character: Character) {
        disposables += charactersInteractor.saveCharacter(character)
            .observeOn(mainThread())
            .subscribeBy(onComplete = {
                router.navigateTo(Screens.Character(character.name))
            })
    }

    fun loadNextPage() {
        paginator.loadNewPage()
    }

    companion object {
        private const val DEBOUNCE_TIMEOUT_MS = 400L
    }
}
