package com.starwars.characters.ui.root

import android.arch.lifecycle.ViewModel
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class RootViewModel @Inject constructor(private val router: Router) : ViewModel() {

    init {
        openSearch()
    }

    fun openRecent() {
        router.newRootScreen(Screens.Recent())
    }

    fun openSearch() {
        router.newRootScreen(Screens.Search())
    }
}