package com.starwars.characters.ui.utils

import android.support.annotation.IdRes
import android.view.View
import android.widget.ViewAnimator

fun ViewAnimator.setDisplayedChildId(@IdRes id: Int) {
    for (i in 0 until childCount) {
        val child = getChildAt(i)

        if (child.id == id && i != displayedChild) {
            displayedChild = i
            break
        }
    }
}

fun View.isVisible(visible: Boolean, invisibilityMode: Int = View.GONE) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        invisibilityMode
    }
}