package com.starwars.characters.ui.search

import android.os.Handler
import android.os.Looper
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.starwars.characters.business.models.Character
import com.starwars.characters.ui.utils.delegates.CharacterAdapterDelegate
import com.starwars.characters.ui.utils.delegates.ProgressAdapterDelegate
import com.starwars.characters.ui.utils.delegates.ProgressItem

class CharactersPagedAdapter(
    private val nextPageListener: () -> Unit,
    onClick: (character: Character) -> Unit
) : ListDelegationAdapter<MutableList<Any>>() {

    private val mainHandler = Handler(Looper.getMainLooper())

    init {
        items = mutableListOf()
        delegatesManager.addDelegate(CharacterAdapterDelegate(onClick))
        delegatesManager.addDelegate(ProgressAdapterDelegate())
    }

    fun setData(characters: List<Character>) {
        mainHandler.post {
            val progress = isProgress()
            val oldItems = items.toList()

            items.clear()
            items.addAll(characters)

            if (progress) {
                items.add(ProgressItem())
            }

            DiffUtil
                .calculateDiff(DiffCallback(items, oldItems), false)
                .dispatchUpdatesTo(this)
        }
    }

    fun clear() {
        setData(emptyList())
    }

    fun showProgress(isVisible: Boolean) {
        mainHandler.post {
            val progress = isProgress()

            if (isVisible && !progress) {
                items.add(ProgressItem())
                notifyItemInserted(items.lastIndex)
            } else if (!isVisible && progress) {
                items.remove(items.last())
                notifyItemRemoved(items.size)
            }
        }
    }

    private fun isProgress() = items.isNotEmpty() && items.last() is ProgressItem

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
        super.onBindViewHolder(holder, position, payloads)

        val prefetchPosition = items.size - PREFETCH_DISTANCE
        if (position == prefetchPosition) {
            nextPageListener()
        }
    }

    private class DiffCallback(
        private val newItems: List<Any>,
        private val oldItems: List<Any>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return oldItem is Character && newItem is Character && oldItem.name == newItem.name
        }

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = true
    }

    companion object {
        private const val PREFETCH_DISTANCE = 10
    }
}