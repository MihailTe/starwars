package com.starwars.characters.ui.character

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starwars.characters.R
import com.starwars.characters.di.provideViewModel
import com.starwars.characters.ui.base.BaseFragment
import com.starwars.characters.ui.utils.ScreenState
import com.starwars.characters.ui.utils.setDisplayedChildId
import kotlinx.android.synthetic.main.fragment_character.*
import kotlinx.android.synthetic.main.view_error.*

class CharacterFragment : BaseFragment() {

    private lateinit var viewModel: CharacterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel()
        viewModel.init(arguments?.getString(NAME_BUNDLE_KEY)!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_character, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar.setNavigationOnClickListener { viewModel.close() }

        retryButton.setOnClickListener { viewModel.retry() }

        bindViewState()
    }

    private fun bindViewState() {
        viewModel.getViewState()
            .observe(this, Observer {
                when (it) {
                    is CharacterViewModel.ViewState.Loading -> {
                        viewAnimator.setDisplayedChildId(R.id.progress)
                        toolbar.title = it.characterName
                    }
                    is CharacterViewModel.ViewState.Error -> viewAnimator.setDisplayedChildId(R.id.errorStub)
                    is CharacterViewModel.ViewState.CharacterInfo -> {
                        it.value
                            .let {
                                name.text = it.name
                                height.text = it.height
                                mass.text = it.mass
                                hairColor.text = it.hairColor
                                skinColor.text = it.skinColor
                                eyeColor.text = it.eyeColor
                                birthYear.text = it.birthYear
                                gender.text = it.gender
                            }

                        viewAnimator.setDisplayedChildId(R.id.info)
                    }
                }
            })
    }

    companion object {
        private const val NAME_BUNDLE_KEY = "NAME_BUNDLE_KEY"

        fun newInstance(name: String): CharacterFragment {
            val bundle = Bundle().apply { putString(NAME_BUNDLE_KEY, name) }
            return CharacterFragment().also { it.arguments = bundle }
        }
    }
}