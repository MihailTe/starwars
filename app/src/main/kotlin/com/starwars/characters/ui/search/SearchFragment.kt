package com.starwars.characters.ui.search

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starwars.characters.R
import com.starwars.characters.di.provideViewModel
import com.starwars.characters.ui.base.BaseFragment
import com.starwars.characters.ui.utils.Paginator.ScreenState
import com.starwars.characters.ui.utils.isVisible
import com.starwars.characters.ui.utils.setDisplayedChildId
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.view_error.*

class SearchFragment : BaseFragment() {
    override val hideBottomNavigation: Boolean = false

    private lateinit var viewModel: SearchViewModel

    private lateinit var charactersAdapter: CharactersPagedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel()
        charactersAdapter = CharactersPagedAdapter({ viewModel.loadNextPage() }, { viewModel.openCharacter(it) })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        charactersList.layoutManager = LinearLayoutManager(requireContext())
        charactersList.setHasFixedSize(true)
        charactersList.adapter = charactersAdapter

        queryField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                val query = s.toString()

                clearButton.isVisible(query.isNotEmpty(), View.INVISIBLE)

                if (viewModel.getLastQuery() != query) {
                    viewModel.setQuery(query)
                    charactersAdapter.clear()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        clearButton.setOnClickListener { queryField.text = null }

        retryButton.setOnClickListener { viewModel.retry() }

        viewModel.getScreenState()
            .observe(this, Observer {
                val isProgress = it is ScreenState.EmptyProgress || it is ScreenState.PageProgress
                charactersAdapter.showProgress(isProgress)

                when (it) {
                    is ScreenState.Empty -> {
                        if (viewModel.isEmptyQuery()) {
                            viewAnimator.setDisplayedChildId(R.id.emptyQuery)
                        } else {
                            viewAnimator.setDisplayedChildId(R.id.emptyResult)
                        }
                    }
                    is ScreenState.Error -> {
                        viewAnimator.setDisplayedChildId(R.id.errorStub)
                    }
                    is ScreenState.PageProgress, is ScreenState.EmptyProgress -> {
                        viewAnimator.setDisplayedChildId(R.id.charactersList)
                    }
                    is ScreenState.Data -> {
                        charactersAdapter.setData(it.value)
                        viewAnimator.setDisplayedChildId(R.id.charactersList)
                    }
                }
            })
    }
}