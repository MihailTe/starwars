package com.starwars.characters.ui.character

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.starwars.characters.business.CharactersInteractor
import com.starwars.characters.business.models.Character
import com.starwars.characters.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposables
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class CharacterViewModel @Inject constructor(
    private val router: Router,
    private val charactersInteractor: CharactersInteractor
) : BaseViewModel() {

    private val viewState = MutableLiveData<ViewState>()

    private var disposable = Disposables.empty()

    private lateinit var name: String

    fun init(name: String) {
        this.name = name
        loadCharacter()
    }

    override fun onCleared() {
        disposable.dispose()
    }

    fun retry() {
        loadCharacter()
    }

    private fun loadCharacter() {
        disposable = charactersInteractor
            .getCharacterByName(name)
            .map<ViewState> { ViewState.CharacterInfo(it) }
            .toObservable()
            .startWith(ViewState.Loading(name))
            .onErrorReturnItem(ViewState.Error)
            .observeOn(mainThread())
            .subscribeBy(
                onNext = {
                    viewState.value = it
                })
    }

    fun close() {
        router.exit()
    }

    fun getViewState(): LiveData<ViewState> = viewState

    sealed class ViewState {
        class Loading(val characterName: String) : ViewState()

        object Error : ViewState()

        class CharacterInfo(val value: Character) : ViewState()
    }
}