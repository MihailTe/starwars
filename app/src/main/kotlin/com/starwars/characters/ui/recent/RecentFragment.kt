package com.starwars.characters.ui.recent

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starwars.characters.R
import com.starwars.characters.di.provideViewModel
import com.starwars.characters.ui.base.BaseFragment
import com.starwars.characters.ui.utils.ScreenState
import com.starwars.characters.ui.utils.setDisplayedChildId
import kotlinx.android.synthetic.main.fragment_recent.*
import kotlinx.android.synthetic.main.view_error.*

class RecentFragment : BaseFragment() {
    override val hideBottomNavigation: Boolean = false

    private lateinit var viewModel: RecentViewModel
    private lateinit var charactersAdapter: CharactersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel()
        charactersAdapter = CharactersAdapter { viewModel.openCharacter(it) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        charactersList.layoutManager = LinearLayoutManager(requireContext())
        charactersList.adapter = charactersAdapter

        retryButton.setOnClickListener { viewModel.retry() }

        viewModel.getCharacters()
            .observe(this, Observer {
                charactersAdapter.setData(it.orEmpty())
            })

        viewModel.getScreenState()
            .observe(this, Observer {
                when (it) {
                    ScreenState.CONTENT -> viewAnimator.setDisplayedChildId(R.id.charactersList)
                    ScreenState.ERROR -> viewAnimator.setDisplayedChildId(R.id.errorStub)
                    ScreenState.LOADING -> viewAnimator.setDisplayedChildId(R.id.progress)
                }
            })
    }
}