package com.starwars.characters.ui.base

import android.support.v4.app.Fragment
import com.starwars.characters.ui.root.RootActivity

open class BaseFragment : Fragment() {
    protected open val hideBottomNavigation: Boolean = true

    override fun onStart() {
        super.onStart()
        if (hideBottomNavigation) {
            (activity as? RootActivity)?.hideBottomNavigation()
        } else {
            (activity as? RootActivity)?.showBottomNavigation()
        }
    }
}