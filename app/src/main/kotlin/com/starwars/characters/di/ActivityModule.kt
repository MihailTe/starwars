package com.starwars.characters.di

import com.starwars.characters.di.mvvm.ViewModelModule
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router


@Module(includes = [ViewModelModule::class])
class ActivityModule {
    private val cicerone = Cicerone.create(Router())

    @Provides
    fun getRouter() = cicerone.router

    @Provides
    fun getNavigationHolder() = cicerone.navigatorHolder
}