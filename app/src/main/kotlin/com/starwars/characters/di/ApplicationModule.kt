package com.starwars.characters.di

import android.arch.persistence.room.Room
import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.starwars.characters.data.db.CharactersDao
import com.starwars.characters.data.db.CharactersDatabase
import com.starwars.characters.data.network.Swapi
import com.starwars.characters.data.repositories.CharactersRepository
import com.starwars.characters.data.repositories.CharactersRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ApplicationModule.Bindings::class])
class ApplicationModule(@get:Provides val context: Context) {

    @Provides
    @Singleton
    fun provideApi(): Swapi {
        val loggingInterceptor = HttpLoggingInterceptor()
            .apply {
                level = HttpLoggingInterceptor.Level.BODY
            }

        val gson = GsonBuilder()
            .setPrettyPrinting()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

        val httpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .baseUrl(API_URL)
            .build()
            .create(Swapi::class.java)
    }

    @Provides
    @Singleton
    fun provideDb(context: Context): CharactersDao {
        return Room
            .databaseBuilder(context, CharactersDatabase::class.java, CharactersDatabase::class.java.simpleName)
            .fallbackToDestructiveMigration()
            .build()
            .getCharactersDao()
    }

    @Module
    interface Bindings {

        @Binds
        fun bindCharactersRepository(repositoryImpl: CharactersRepositoryImpl): CharactersRepository
    }

    companion object {
        private const val API_URL = "https://swapi.co/api/"
    }
}