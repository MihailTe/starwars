package com.starwars.characters.di

import android.app.Application

object Injector {
    private lateinit var applicationComponent: ApplicationComponent
    private lateinit var activityComponent: ActivityComponent

    fun initApplicationComponent(application: Application) {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(application))
            .build()
    }

    fun getActivityComponent(): ActivityComponent {
        if (!::activityComponent.isInitialized) {
            activityComponent = applicationComponent.plus(ActivityModule())
        }

        return activityComponent
    }
}