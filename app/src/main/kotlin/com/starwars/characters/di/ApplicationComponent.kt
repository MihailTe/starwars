package com.starwars.characters.di

import android.content.Context
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun getContext(): Context

    fun plus(activityModule: ActivityModule): ActivityComponent
}