package com.starwars.characters.di

import com.starwars.characters.di.mvvm.ViewModelFactory
import dagger.Subcomponent
import ru.terrakok.cicerone.NavigatorHolder

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {

    fun getViewModelFactory(): ViewModelFactory

    fun getNavigationHolder(): NavigatorHolder
}