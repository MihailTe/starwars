package com.starwars.characters.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment

inline fun <reified T : ViewModel> Fragment.provideViewModel() =
    ViewModelProviders
        .of(this, Injector.getActivityComponent().getViewModelFactory())
        .get(T::class.java)